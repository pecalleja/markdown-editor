
def plain():
    text = input("Text:")
    return text


def bold():
    text = input("Text:")
    return f"**{text}**"


def italic():
    text = input("Text:")
    return f"*{text}*"


def header():
    level = int(input("Level:"))
    if not (1 <= level <= 6):
        print("The level should be within the range of 1 to 6")
        return header()
    text = input("Text:")
    header_text = "#"*level
    return f"{header_text} {text}\n"


def link():
    label = input("Label:")
    url = input("URL:")
    return f"[{label}]({url})"


def new_line():
    return "\n"


def inline_code():
    text = input("Text:")
    return f"`{text}`"


def some_list(ordered=True):
    rows = int(input("Number of rows:"))
    if rows <= 0:
        print("The number of rows should be greater than zero")
        return some_list(ordered)
    row_lines = ""
    for i in range(1, rows+1):
        line = input(f"Row #{i}:")
        if ordered:
            start_line = f"{i}."
        else:
            start_line = "*"
        row_lines += f"{start_line} {line}\n"
    return row_lines


def unordered_list():
    return some_list(ordered=False)


def ordered_list():
    return some_list(ordered=True)


class MarkdownEditor:
    def __init__(self):
        self.formats = {
            "plain": plain,
            "bold": bold,
            "italic": italic,
            "header": header,
            "link": link,
            "new-line": new_line,
            "inline-code": inline_code,
            "unordered-list": unordered_list,
            "ordered-list": ordered_list
        }
        self.commands = ("!help", "!done")
        self.text = ""

    @property
    def commands_formats(self):
        return tuple(self.formats.keys()) + self.commands

    def start(self):
        result = False
        while result is False:
            command_formatter = input("Choose a formatter:")
            if command_formatter not in self.commands_formats:
                print("Unknown formatting type or command")
            elif command_formatter == "!help":
                self.help()
            elif command_formatter == "!done":
                result = self.save()
            else:
                action = self.formats.get(command_formatter)
                self.text += action()
                print(self.text)

    def save(self):
        with open("output.md", mode="w+") as file:
            file.write(self.text)
        return True

    def help(self):
        print("Available formatters: plain bold italic header link inline-code ordered-list unordered-list new-line")
        print("Special commands: !help !done")


MarkdownEditor().start()
